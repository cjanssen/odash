O--->

A game by Michael Hussinger and Christiaan Janssen.
Made at the Global Game Jam 2014 in Berlin.


Controls:
WASD / cursor keys  - move
Mouse  -  look at
ESC -  Exit
ALT + RETURN - Toggle fullscreen


Code + Art License:
Creative Commons 3.0 by-sa-nc
see http://creativecommons.org/licenses/by-nc-sa/3.0/


Music:
"Sousa's Thunderer March" by Free Tim
Public Domain license
http://freemusicarchive.org/music/Free_Tim/Sousas_Thunderer_March/Sousas_Thunderer_March

