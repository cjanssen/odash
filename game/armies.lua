function initArmies()
	armies = {}

	armies.white = {}
	armies.black = {}
	armies[1] = armies.white
	armies[2] = armies.black
	armies.white.color = {255,255,255}
	armies.black.color = {0,0,0}

	spawnPoints = { {2,1}, {4,1}, {0,2}, {0,4}, {0,6}, {0,8}, {0,10}, {2,11}, {4,11} }
	sizeArray = { 16, 24, 32, 40, 48, 56, 64, 72, 80, 88 }

	armies.limitPairs = 10

	attackFactor = 1/3
	lifeFactor = 2


	initArmyGraphics()
end

function startTimer()
	local t = love.timer.getTime()
	local p = 2
	local predelay = - 0.15
	timer = {
		refT = t,
		nextT = t+p+predelay,
		periodT = p
	}
end

function checkTimer()
	local t = love.timer.getTime()
	if t > timer.nextT then
		timer.nextT = timer.nextT + timer.periodT
		return true
	end
	return false
end

function timeToBeat()
	local t = love.timer.getTime()
	return timer.nextT - t
end

function initArmyGraphics()
	littleBarImg = love.graphics.newImage("img/minibar_frame2.png")

	local img = love.graphics.newImage("img/soldier_feetsheet2.png")
	armies.feetsprite_batch = love.graphics.newSpriteBatch(img,8+14,"static") -- usagehint?
	armies.feetsprites = { walking = {}, standing = {} }
	armies.feetsprites[1] = armies.feetsprites.walking
	armies.feetsprites[2] = armies.feetsprites.standing

	for i=0,7 do
		table.insert(armies.feetsprites.standing,
			love.graphics.newQuad(i*64, 5*64, 64, 64, img:getWidth(), img:getHeight()))
	end

	for ii = 0,13 do
		local i = ii % 8
		local j = math.floor(ii/8)
		table.insert(armies.feetsprites.walking,
			love.graphics.newQuad(i*64, (7-j)*64, 64, 64, img:getWidth(), img:getHeight()))
	end

	local img = love.graphics.newImage("img/soldierwhite_body2.png")

	armies.white.bodysprite_batch = love.graphics.newSpriteBatch(img, 10, "static")
	armies.white.sprites = {}

	for j=0,9 do
		armies.white.sprites[j+1] = {}
		table.insert(armies.white.sprites[j+1],
			love.graphics.newQuad(0, j*128, 128, 128, img:getWidth(), img:getHeight()))
	end

	local img = love.graphics.newImage("img/soldierwhite_body_lookat.png")

	armies.white.bodysprite_batch_lookat = love.graphics.newSpriteBatch(img, 10, "static")
	armies.white.sprites_lookat = {}

	for j=0,9 do
		armies.white.sprites_lookat[j+1] = {}
		table.insert(armies.white.sprites_lookat[j+1],
			love.graphics.newQuad(0, j*128, 128, 128, img:getWidth(), img:getHeight()))
	end

	local img = love.graphics.newImage("img/soldierblack_body2.png")
	armies.black.bodysprite_batch = love.graphics.newSpriteBatch(img, 10, "static")
	armies.black.sprites = {}
	for j=0,9 do
		armies.black.sprites[j+1] = {}
		table.insert(armies.black.sprites[j+1],
			love.graphics.newQuad(0, j*128, 128, 128, img:getWidth(), img:getHeight()))
	end

	local img = love.graphics.newImage("img/soldierblack_body_lookat.png")
	armies.black.bodysprite_batch_lookat = love.graphics.newSpriteBatch(img, 10, "static")
	armies.black.sprites_lookat = {}
	for j=0,9 do
		armies.black.sprites_lookat[j+1] = {}
		table.insert(armies.black.sprites_lookat[j+1],
			love.graphics.newQuad(0, j*128, 128, 128, img:getWidth(), img:getHeight()))
	end

end

function startArmies()
	-- for i=1,2 do
	-- 	armies[i].list = {}
	-- end
	armies.list = {}
	armies.pairCount = 0

	-- balance
	armies.maxbalance = 20
	armies.balance = armies.maxbalance / 2

	-- start with 5 couples
	for i = 1,4 do
		spawnCouple()
	end
end

function getDist( posA, posB )
	local dx = posB.x - posA.x
	local dy = posB.y - posA.y
	return math.sqrt(dx*dx + dy*dy)
end

function min(a,b)
	if a<b then return a else return b end
end

function max(a,b)
	if a>b then return a else return b end
end

function spawnCouple()
	if armies.pairCount >= armies.limitPairs then return end

	-- choose size (biased towards dark)
	local sizeA = math.random(10)
	local sizeB = min(math.random(7) + math.random(5), 10)
	-- local sizeB = math.random(9) + 1

	local baseFeetDelay = 0.05
	local baseBodyDelay = 0.15
	local baseStandDelay = 2/16


	-- choose spawn point
	local sp = math.random(table.getn(spawnPoints))
	local origA = { x = spawnPoints[sp][1]*64, y = spawnPoints[sp][2]*64 }
	sp = math.random(table.getn(spawnPoints))
	local origB = { x = screenSize.x - spawnPoints[sp][1]*64, y = spawnPoints[sp][2]*64 }

	local validPoint = false
	local meetRow, meetCol, meetPoint
	local tries = 12
	while not validPoint do
		validPoint = true
		meetRow = math.random(8) - 1
		meetCol = math.random(6) - 1
		meetPoint = { x = 64* (3+meetCol*2+(meetRow%2)), y = 64 * (3 + meetRow) }

		if meetRow == 3 and meetCol == 3 then validPoint = false end
		if meetRow % 2 == 1 and meetCol == 5 then validPoint = false end
		for i,v in ipairs(armies.list) do
			if v.armyIndex == 1 and v.mp and getDist( meetPoint, v.mp ) < 64*2.5 then
				validPoint = false
			end
		end
		tries = tries - 1
		if tries <= 0 then return end
	end

	-- choose destination
	local pixA = sizeArray[sizeA]
	local pixB = sizeArray[sizeB]
	local destA = { x = meetPoint.x - pixA, y = meetPoint.y - 32 }
	local destB = { x = meetPoint.x + pixB, y = meetPoint.y - 33 }

	local distA = getDist( origA, destA )
	local distB = getDist( origB, destB )
	local speedA = 200
	local speedB = speedA * distB / distA
	if speedB > speedA then
		speedB = 200
		speedA = speedB * distA / distB
	end

	-- bar position
	local barpos = { x = (destA.x + destB.x)/2, y = meetPoint.y - 24 - max(sizeArray[sizeA],sizeArray[sizeB]) }


	local jd = 0.15
	local alpha = 0.9

	-- attack/life
	local devA = 0.2 * (math.random()-0.5)
	local devB = 0.2 * (math.random()-0.5)

	local soldierA = {
		pos = origA,
		dir = {x = (destA.x - origA.x)/distA, y = (destA.y - origA.y)/distA},
		speed = speedA,
		meetTime = distA / speedA,

		sizeIndex = sizeA,
		size = sizeArray[sizeA],
		life = sizeA * lifeFactor,
		lifeExtra = 0,

		origSizeIndex = sizeA,
		origSize = sizeArray[sizeA],

		attack = sizeA * attackFactor + devA,
		attackDev = devA,

		state = 1,
		armyIndex = 1,

		mp = { x = meetPoint.x, y = meetPoint.y },
		baroffset = barpos,

		feetSpriteIndex = 0,
		feetSpriteTime = 0,
		feetSpriteDelay = { baseFeetDelay * 200 / speedA, baseStandDelay },
		bodySpriteIndex = 0,
		bodySpriteTime = 0,
		bodySpriteDelay = baseBodyDelay,

		-- jump
		jumpPhase = 0,
		drag = alpha,
		jumpTime = 0,
		jumpDelay = jd,
		jumpSpeed = pixA / 2 / jd,

		bounceDelay = 2,
		bounceTime = 0,
		bounceStartSpeed = -pixA / 2 / jd,
		bounceSpeed = 0,

		watched = false,
		wasWatched = false,
		watchTimer = 0,
		watchDelay = 0.6,
		active = false,

		bodyOfsPhase = math.pi/2 + math.pi/16,
		bodyOfsStartPhase = math.pi/2 + math.pi/16,
		bodyOfsFreq = math.pi*2,
		bodyOfs = { x = 0, y = 0 },
		bodyOfsBase = { x = 0, y = 5 },
		bodyOfsAmp = { x = 6, y = -6 }


	}


	local soldierB = {
		pos = origB,
		dir =  {x = (destB.x - origB.x)/distB, y = (destB.y - origB.y)/distB},
		speed = speedB,
		meetTime = distB / speedB,

		sizeIndex = sizeB,
		size = sizeArray[sizeB],
		origSizeIndex = sizeB,
		origSize = sizeArray[sizeB],

		life = sizeB * lifeFactor,
		lifeExtra = 0,
		attack = sizeB * attackFactor + devB,
		attackDev = devB,

		state = 1,
		armyIndex = 2,

		mp = { x = meetPoint.x, y = meetPoint.y },

		feetSpriteIndex = 0,
		feetSpriteTime = 0,
		feetSpriteDelay = { baseFeetDelay * 200 / speedB, baseStandDelay },
		bodySpriteIndex = 0,
		bodySpriteTime = 0,
		bodySpriteDelay = baseBodyDelay,

		jumpPhase = 0,
		drag = alpha,
		jumpTime = 0,
		jumpDelay = jd,
		jumpSpeed = - pixB / 2 / jd,

		bounceDelay = 2,
		bounceTime = 0,
		bounceStartSpeed = pixB / 2 / jd,
		bounceSpeed = 0,

		watched = false,
		wasWatched = false,
		watchTimer = 0,
		watchDelay = 0.6,
		active = false,

		bodyOfsPhase = math.pi/2 + math.pi/16,
		bodyOfsStartPhase = math.pi/2 + math.pi/16,
		bodyOfsFreq = -math.pi*2,
		bodyOfs = { x = 0, y = 0 },
		bodyOfsBase = { x = 0, y = 5 },
		bodyOfsAmp = { x = 6, y = -6 }

	}

	soldierA.partner = soldierB
	soldierB.partner = soldierA
	-- table.insert(armies.white.list, soldierA)
	-- table.insert(armies.black.list, soldierB)
	table.insert(armies.list, soldierA)
	table.insert(armies.list, soldierB)
	armies.pairCount = armies.pairCount + 1
end

function absarcdiff(angleA, angleB)
	local tmpang = math.abs((angleA - angleB)% (2*math.pi))
	if tmpang > math.pi then tmpang = math.pi * 2 - tmpang end
	return tmpang
end

function updateArmies(dt)
	-- update heart
	local heartbeat = checkTimer()
	if heartbeat then
		if gameState ~= 3 and math.random() < 0.6 then
			spawnCouple()
		end
	end

	-- update soldiers
	for i,v in ipairs(armies.list) do
		if v.isPlayer then
			updatePlayer(dt)
		elseif v.isDeathAnim then
			updateDeathAnim(v,dt)
		else
			v.watched = false

			if v.state == 1 then
				-- approach
				v.pos.x = v.pos.x + v.dir.x * v.speed * dt
				v.pos.y = v.pos.y + v.dir.y * v.speed * dt

				v.centerPos = { x = v.pos.x, y = v.pos.y }

				v.meetTime = v.meetTime - dt
				if v.meetTime <= 0 then
					v.speed = 0
					v.state = 2
					v.dir.x = (1.5 - v.armyIndex) * 2
					v.feetSpriteIndex = (31 - math.floor(timeToBeat()/v.feetSpriteDelay[2]))%8 + 1
				end
			elseif v.state == 2 then
				-- watched?
				local dx = v.pos.x - player.centerPos.x
				local dy = v.pos.y - player.centerPos.y
				local angleToPlayer = math.atan2(dy,dx)
				local distToPlayer = math.sqrt(dx*dx+dy*dy)
				local arcPermitted = math.asin((v.size + player.beamwidth)/2 / distToPlayer)
				if absarcdiff(angleToPlayer, player.pointangle) <= arcPermitted then
					v.watched = true
				end

				-- activate when watched long enough
				if v.watched and not v.wasWatched then
					v.watchTimer = v.watchDelay
				end

				if v.wasWatched and not v.watched then
					v.watchTimer = v.watchDelay/2
				end

				if v.watchTimer > 0 then
					v.watchTimer = v.watchTimer - dt
					if v.watchTimer <= 0 then
						v.active = v.watched
					end
				end

				v.wasWatched = v.watched

				-- no growth when endgame
				if gameState == 3 then v.active = false end

				-- resize if watched
				if v.active then
					v.sizeIndex = player.sizeIndex
					v.size = sizeArray[v.sizeIndex]
					v.lifeExtra = (v.sizeIndex - v.origSizeIndex) * lifeFactor
					v.attack = v.sizeIndex * attackFactor + v.attackDev
				else
					v.sizeIndex = v.origSizeIndex
					v.size = sizeArray[v.origSizeIndex]
					v.lifeExtra = 0
					v.attack = v.origSizeIndex * attackFactor + v.attackDev
				end

				-- fight
				if heartbeat and gameState ~= 3 then
					v.jumpPhase = 1
					v.jumpTime = v.jumpDelay
					v.pos.x = v.centerPos.x
					v.pos.y = v.centerPos.y
				end

				-- jump
				if v.jumpPhase == 0 then
					v.pos.x = v.centerPos.x
					v.pos.y = v.centerPos.y
				elseif v.jumpPhase == 1 then
					v.jumpTime = v.jumpTime - dt
					if v.jumpTime <= 0 then
						v.jumpPhase = 2
						v.bounceTime = v.bounceDelay
						v.bounceSpeed = v.bounceStartSpeed


						-- hit moment
						if v.armyIndex == 1 then
							addStar({x = v.mp.x, y = v.mp.y - 24 })
						end
						v.partner.life = v.partner.life - v.attack
						if v.partner.life + v.partner.lifeExtra <= 0 then
							v.partner.dead = true
							v.dead = true

							armies.pairCount = armies.pairCount - 1
							if v.armyIndex==1 then -- white
								if gameState ~= 3 then
									armies.balance = armies.balance + 1
								end
								addConfetti(v.mp, 1)
								-- player grow
								if v.active or v.partner.active then
									player.sizeIndex = player.sizeIndex + 1
									if player.sizeIndex > 10 then
										player.sizeIndex = 10
									end
								end

								-- endgame : win
								if armies.balance >= armies.maxbalance then
									gameState = 3
								end
							else
								if gameState ~= 3 then
									armies.balance = armies.balance - 1
								end
								addConfetti(v.mp, 2)

								-- player shrink
								if v.active or v.partner.active then
									player.sizeIndex = player.sizeIndex - 1
									if player.sizeIndex < 1 then
										player.sizeIndex = 1
									end
								end

								-- endgame : lose
								if armies.balance <= 0 then
									gameState = 3
								end
							end
							addDeathAnim(v.centerPos, v.armyIndex, v.sizeIndex)
							addDeathAnim(v.partner.centerPos, v.partner.armyIndex, v.partner.sizeIndex)
						end
					end

					v.pos.x = v.pos.x + v.jumpSpeed * dt
				elseif v.jumpPhase == 2 then
					v.bounceTime = v.bounceTime - dt
					if v.bounceTime <= 0 then
						v.jumpPhase = 0
					end
					v.pos.x = v.pos.x + v.bounceSpeed * dt
					v.bounceSpeed = v.bounceSpeed * math.pow(v.drag, dt*60)
				end
			end


			-- animations
			v.feetSpriteTime = v.feetSpriteTime - dt
			-- special: synch feet to beat
			if heartbeat and v.state == 2 then
				v.feetSpriteTime = 0
				v.feetSpriteIndex = 0
			end
			if v.feetSpriteTime <= 0 then
				v.feetSpriteTime = v.feetSpriteTime + v.feetSpriteDelay[v.state]
				v.feetSpriteIndex = v.feetSpriteIndex + 1
			end
			if v.feetSpriteIndex > table.getn(armies.feetsprites[v.state]) then
				v.feetSpriteIndex = 1
			end

			v.bodySpriteTime = v.bodySpriteTime - dt
			if v.bodySpriteTime <= 0 then
				v.bodySpriteTime = v.bodySpriteTime + v.bodySpriteTime
				v.bodySpriteIndex = v.bodySpriteIndex + 1
				local m = table.getn(armies[v.armyIndex].sprites[v.sizeIndex])
				if v.bodySpriteIndex > m then
					v.bodySpriteIndex = m
				end
			end

			if heartbeat then
				v.bodySpriteIndex = 1
			end

			-- body move
			v.bodyOfsPhase = v.bodyOfsPhase + v.bodyOfsFreq * dt
			if heartbeat then
				v.bodyOfsPhase = v.bodyOfsStartPhase
			end

			local bos = math.sin(v.bodyOfsPhase)
			local bosx = (1-bos*bos)
			local boss = signOf(math.cos(v.bodyOfsPhase))
			local ampfac = math.log(v.sizeIndex+1)/2
			v.bodyOfs.x = v.bodyOfsBase.x + v.bodyOfsAmp.x * bosx * boss * ampfac
			v.bodyOfs.y = v.bodyOfsBase.y + v.bodyOfsAmp.y * bos * bos * ampfac
		end
	end


	if unexpected_condition then return end
	local n = table.getn(armies.list)
	for i = 0,n-1 do
		if armies.list[n-i] and armies.list[n-i].dead then
			table.remove(armies.list,n-i)
		end
	end

	-- z-sort
	-- hack: ignore errors and continue (no time to debug now)
	table.sort(armies.list, getZPos)
end

function signOf(a)
	if a < 0 then return -1 end
	return 1
end

function getZPos(a,b)
	if a and a.pos and a.pos.y and b and b.pos and b.pos.y then
		return a.pos.y < b.pos.y
	else
		return false
	end
end

function drawBatch(batch, picndx, posx, posy, angle, hscale, vscale, centerx, centery)
	batch:clear()
	batch:add(picndx, posx, posy, angle, hscale, vscale, centerx, centery)
	love.graphics.draw(batch)
end

function drawArmies()
	love.graphics.setColor(255,255,255)

	for i,v in ipairs(armies.list) do
		if v.isPlayer then
			drawPlayer()
		elseif v.isDeathAnim then
			drawDeathAnim(v)
		else
			love.graphics.setColor(255,255,255)
			local lpos = { x = math.floor(v.pos.x), y =math.floor(v.pos.y) }

			-- feet
			local hmirror = 1
			if v.dir.x < 0 then hmirror = -1 end

			drawBatch(armies.feetsprite_batch, armies.feetsprites[v.state][v.feetSpriteIndex], lpos.x, lpos.y+9, 0, hmirror, 1, 32, 0)

			local bodypos = { x = math.floor(v.pos.x + v.bodyOfs.x), y = math.floor(v.pos.y + v.bodyOfs.y) }

			-- body
			if v.armyIndex == 1 then hmirror = 1 else hmirror = -1 end
			if not v.watched then
				drawBatch(armies[v.armyIndex].bodysprite_batch, armies[v.armyIndex].sprites[v.sizeIndex][v.bodySpriteIndex], bodypos.x, bodypos.y, 0, hmirror, 1, 64, 64)
			else
				drawBatch(armies[v.armyIndex].bodysprite_batch_lookat, armies[v.armyIndex].sprites[v.sizeIndex][v.bodySpriteIndex], bodypos.x, bodypos.y, 0, hmirror, 1, 64, 64)
			end
		end
	end



	-- fight status bar
	for i,v in ipairs(armies.list) do
		if not v.isPlayer and v.armyIndex == 1 and v.state == 2 then
			local selfLife = max(v.life + v.lifeExtra, 0)
			local otherLife = max(v.partner.life + v.partner.lifeExtra, 0)
			local lifefrac = selfLife/(selfLife + otherLife)
			local barlen = 98
			local ox = v.baroffset.x - barlen/2
			local oy = v.baroffset.y
			love.graphics.setColor(208,167,52)
			love.graphics.rectangle("fill", ox, oy, barlen*lifefrac, 9)
			love.graphics.setColor(95,37,44)
			love.graphics.rectangle("fill", ox + barlen*lifefrac, oy, barlen*(1-lifefrac), 9)
			love.graphics.setColor(255,255,255)
			love.graphics.draw(littleBarImg, ox - 11, oy-16)
		end
	end
end

function drawArmiesBar()

	-- armies status bar
	local frac = armies.balance / armies.maxbalance
	love.graphics.setColor(208,167,52)
	love.graphics.rectangle("fill", 0, 11, screenSize.x * frac, 42)
	love.graphics.setColor(95,37,44)
	love.graphics.rectangle("fill", screenSize.x * frac, 11, screenSize.x * (1-frac), 42)
end
