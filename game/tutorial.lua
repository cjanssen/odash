function initTutorial()
	Tutorial = {
		pages = {
			love.graphics.newImage("img/titlescreen2.png"),
			love.graphics.newImage("img/tutorial1_upward2.png"),
			love.graphics.newImage("img/tutorial2_upward.png"), 
			love.graphics.newImage("img/tutorial3_right.png"),
			love.graphics.newImage("img/tutorial4_right.png"),
			love.graphics.newImage("img/tutorial5_left.png")
		}
	}

	Tutorial.count = table.getn(Tutorial.pages)
	Tutorial.openAll = false
end

function startTutorial()
	Tutorial.visible = true
	Tutorial.currentPage = 1
	Tutorial.offsets = {{x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}}
	Tutorial.dirs = {{x=0, y=-1}, {x = 0, y = -1}, {x = 0, y = -1}, {x = 1, y = 0}, {x = 1, y = 0}, {x = -1, y = 0} }
	Tutorial.visibility = { true, true, true, true, true, true }


	Tutorial.speed = {}
	for i=0,Tutorial.count do
		if Tutorial.openAll then
			Tutorial.speed[i+1] = 1200 - i * 50
		else
			Tutorial.speed[i+1] = 800
		end
	end
end

function updateTutorial(dt)
	if not Tutorial.visible then return end

	for i=1,Tutorial.currentPage-1 do
		if Tutorial.visibility[i] then
			Tutorial.offsets[i].x = Tutorial.offsets[i].x + Tutorial.speed[i] * Tutorial.dirs[i].x * dt
			Tutorial.offsets[i].y = Tutorial.offsets[i].y + Tutorial.speed[i] * Tutorial.dirs[i].y * dt

			if Tutorial.offsets[i].x < -screenSize.x * 1.5 or Tutorial.offsets[i].x > screenSize.x * 1.5 then
				Tutorial.visibility[i] = false
			end
			if Tutorial.offsets[i].y < -screenSize.y * 1.5 or Tutorial.offsets[i].y > screenSize.y * 1.5 then
				Tutorial.visibility[i] = false
			end
		end
	end

	Tutorial.visible = false
	for i=1,Tutorial.count do
		Tutorial.visible = Tutorial.visible or Tutorial.visibility[i]
	end
	if not Tutorial.visible then Tutorial.openAll = true end
end

function tutorialClicked()
	if not Tutorial.visible then return end

	Tutorial.currentPage = min(Tutorial.currentPage + 1, Tutorial.count+1)
	if Tutorial.openAll then Tutorial.currentPage = Tutorial.count+1 end
end

function drawTutorial()
	if not Tutorial.visible then return end

	love.graphics.setColor(255,255,255)
	local n = table.getn(Tutorial.pages)
	for i=0,n-1 do
		if Tutorial.visibility[n-i] then
			love.graphics.draw(Tutorial.pages[n-i], Tutorial.offsets[n-i].x, Tutorial.offsets[n-i].y)
		end
	end
end