PlayerControl = {}

function initPlayer()
	player = {}
	love.graphics.setLineWidth(20)
	initPlayerFrames()
end

function initPlayerFrames()
	local img = love.graphics.newImage("img/player_sheet.png")
	PlayerFrames = {}
	PlayerBatch = love.graphics.newSpriteBatch(img,10*10,"static")

	for j=0,9 do
		PlayerFrames[j+1] = {}
		for i=0,9 do
			table.insert(PlayerFrames[j+1],
				love.graphics.newQuad(i*128, j*128, 128, 128, img:getWidth(), img:getHeight()))
		end
	end

	PlayerFrames.laser = love.graphics.newImage("img/laser.png")
end

function startPlayer()
	player = {
		isPlayer = true,
		pos = { x = screenSize.x/2, y = screenSize.y/2 },
		centerPos = { x = screenSize.x/2, y = screenSize.y/ 2},
		speed = 2500,
		drag = 0.5,
		dir = {x = 0, y = 0},
		acc = 2.5,
		vel = {x = 0, y = 0},

		sizeIndex = 5,

		pointangle = 0,
		beamwidth = 48,
		laserOfs = 0,
		laserSpd = -15,
		opacity = 1,
		opacityFactor = 0.95
	}
	table.insert(armies.list, player)

end

function ndxForAngle(ang)
	if ang > math.pi + math.pi / 16 or ang < -math.pi/16 then
		return 1
	end

	local n =  math.floor ( ( math.pi + math.pi / 16 - ang ) / (math.pi / 8) )
	return n + 2
end

function updatePlayer(dt)
	if gameState == 3 then
		player.opacity = player.opacity * math.pow(player.opacityFactor, dt*60)
		return
	end
	PlayerControl.evalKeys()
	PlayerControl.evalMouse()
	PlayerControl.move(dt)
	PlayerControl.updateLaser(dt)
end

function drawPlayer()
	love.graphics.setColor(255,255,255, player.opacity * 255)
	drawBatch(PlayerBatch, PlayerFrames[player.sizeIndex][ndxForAngle(player.pointangle)], math.floor(player.pos.x), math.floor(player.pos.y), 0, 1, 1, 64, 64)
	drawLaser()
end

function laserStencil()
	love.graphics.rectangle("fill", -32, 16 + 4 * player.sizeIndex, 64, 2048)
end

function drawLaser()
	love.graphics.push()
	love.graphics.translate(math.floor(player.centerPos.x), math.floor(player.centerPos.y))
	love.graphics.rotate(player.pointangle - math.pi/2)
	love.graphics.stencil(laserStencil,"replace",1)
	love.graphics.setStencilTest("greater",0)
	for i=0,7 do
		love.graphics.draw(PlayerFrames.laser, -32, math.floor(i*256 - player.laserOfs))
	end
	love.graphics.setStencilTest()
	love.graphics.pop()

end

function normalize(vec)
	local value = math.sqrt(vec.x*vec.x + vec.y*vec.y)
	if value == 0 then
	 	return {x = vec.x, y = vec.y}
	end
	return {x = vec.x / value, y = vec.y / value}
end

function getModulo(vec)
	return math.sqrt(vec.x*vec.x + vec.y*vec.y)
end

function PlayerControl.move(dt)
	local p = player
	local drag = math.pow(p.drag,dt*60)

	p.vel = { x = p.vel.x * drag + p.dir.x * p.acc * dt,
			  y = p.vel.y * drag + p.dir.y * p.acc * dt }

	p.pos = { x = p.pos.x + p.vel.x * p.speed * dt,
			  y = p.pos.y + p.vel.y * p.speed * dt }

	if p.pos.x < 0 then p.pos.x = 0 end
	if p.pos.x > screenSize.x then p.pos.x = screenSize.x end
	if p.pos.y < 0 then
		p.pos.y = 0
		if p.vel.y < 0 then p.vel.y = 0 end
	end
	if p.pos.y > screenSize.y then
		p.pos.y = screenSize.y
		if p.vel.y > 0 then p.vel.y = 0 end
	end

	p.centerPos.x = p.pos.x
	p.centerPos.y = p.pos.y + (11-p.sizeIndex) * 4
end

function PlayerControl.updateLaser(dt)
	player.laserOfs = (player.laserOfs + dt * player.laserSpd) % 256
end

function PlayerControl.evalKeys()
	local dir = { x = 0, y = 0 }
	dir.x = 0
	if love.keyboard.isDown("left") or love.keyboard.isDown("a") then
		dir.x = -1
	end

	if love.keyboard.isDown("right")  or love.keyboard.isDown("d") then
		dir.x = 1
	end

	dir.y = 0
	if love.keyboard.isDown("up")  or love.keyboard.isDown("w") then
		dir.y = -1
	end

	if love.keyboard.isDown("down")  or love.keyboard.isDown("s") then
		dir.y = 1
	end

	player.dir = normalize(dir)

end

function PlayerControl.evalMouse()
	local dx = love.mouse.getX() - player.centerPos.x
	local dy = love.mouse.getY() - player.centerPos.y
	player.pointangle = math.atan2(dy,dx)
end