function initStars()
	Stars = {}
	Stars.frame = love.graphics.newImage("img/painstar.png")
end

function startStars()
	Stars.list = {}
end

function addStar(startPos)
	ang = -math.pi/2 + (math.random()-0.5)
	table.insert(Stars.list, {
		pos = { x = startPos.x, y = startPos.y },
		dir = { x = math.cos(ang), y = math.sin(ang) },
		speed = 200,
		opacity = 1,
		opacityRate = 0.94,
		angle = math.random() * math.pi*2
		})
end

function updateStars(dt)
	for i,v in ipairs(Stars.list) do
		v.pos.x = v.pos.x + v.dir.x * v.speed * dt
		v.pos.y = v.pos.y + v.dir.y * v.speed * dt
		v.opacity = v.opacity * math.pow(v.opacityRate, dt * 60)
		if v.opacity < 0.01 then
			v.dead = true
		end
	end

	local n = table.getn(Stars.list)
	for i=0,n-1 do
		if Stars.list[n-i].dead then
			table.remove(Stars.list, n-i)
		end
	end
end

function drawStars()
	for i,v in ipairs(Stars.list) do
		love.graphics.setColor(255,255,255,255*v.opacity)
		love.graphics.draw(Stars.frame, math.floor(v.pos.x), math.floor(v.pos.y), v.angle, 1, 1, 32, 32)
	end
end