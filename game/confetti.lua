function initConfetti()
	Confetti = {}
	loadConfettiFrames()
end

function startConfetti()
	Confetti.list = {}
end

function loadConfettiFrames()
	local img = love.graphics.newImage("img/confetti_yellow.png")

	Confetti.frames = {{},{}}
	Confetti.batches = {}
	Confetti.batches[1] = love.graphics.newSpriteBatch(img,16,"static")

	for j=0,3 do
		for i=0,3 do
			table.insert(Confetti.frames[1], 
				love.graphics.newQuad(i*16, (3-j)*16, 16, 16, img:getWidth(), img:getHeight()))
		end
	end

	local img = love.graphics.newImage("img/confetti_red.png")
	Confetti.batches[2] = love.graphics.newSpriteBatch(img,16,"static")

	for j=0,3 do
		for i=0,3 do
			table.insert(Confetti.frames[2], 
				love.graphics.newQuad(i*16, (3-j)*16, 16, 16, img:getWidth(), img:getHeight()))
		end
	end
end

function addConfetti(startPos, color)
	local n = math.random(10) + 20
	for i=1,n do
		local sign = 1
		if math.random() < 0.5 then sign = -1 end

		local ang = (math.log(1-math.random())*sign*1/12-0.5) * math.pi
		local spd = 300 + math.random(200)

		table.insert(Confetti.list, {
			pos = { x = startPos.x, y = startPos.y },
			dir = { x = math.cos(ang), y = math.sin(ang) },
			vel = { x = math.cos(ang) * spd, y = math.sin(ang)*spd },

			grav = 200,
			drag = 0.95,
			osc = 30,

			armyNdx = color,

			opacity = 1,
			opacityFactor = 0.995,

			frameTime = math.random()*0.1,
			frameNdx = math.random(16),
			frameDelay = 0.1
			})
	end
end

function updateConfetti(dt)
	for i,v in ipairs(Confetti.list) do
		-- movement
		v.vel.y = v.vel.y + v.grav * dt

		v.vel.x = v.vel.x * math.pow(v.drag, dt*60) + (math.random()-0.5) * v.osc
		v.vel.y = v.vel.y * math.pow(v.drag, dt*60)

		v.pos.x = v.pos.x + v.vel.x * dt
		v.pos.y = v.pos.y + v.vel.y * dt

		-- opacity
		v.opacity = v.opacity * math.pow(v.opacityFactor, dt * 60)
		if v.opacity < 0.001 then 
			v.dead = true
		end

		-- animation
		v.frameTime = v.frameTime - dt
		while v.frameTime <= 0 do
			v.frameTime = v.frameTime + v.frameDelay
			v.frameNdx = (v.frameNdx % table.getn(Confetti.frames[1])) + 1
		end
	end

	local n = table.getn(Confetti.list)
	for i=0,n-1 do
		if Confetti.list[n-i].dead then
			table.remove(Confetti.list,n-i)
		end
	end
end

function drawConfetti()
	for i,v in ipairs(Confetti.list) do
		love.graphics.setColor(255, 255, 255, 255 * v.opacity)
		drawBatch(Confetti.batches[v.armyNdx], Confetti.frames[v.armyNdx][v.frameNdx], math.floor(v.pos.x), math.floor(v.pos.y))
	end
end