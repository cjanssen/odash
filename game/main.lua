function include( filename )
	love.filesystem.load( filename )()
end

function love.load()
	include("tutorial.lua")
	include("armies.lua")
	include("player.lua")
	include("deathanim.lua")
	include("stars.lua")
	include("confetti.lua")
	include("sound.lua")

	initGame()
	startGame()
end

function initGame()
	screenSize = { x = love.graphics.getWidth(), y = love.graphics.getHeight() }
	isFullscreen = false

	initTutorial()
	initArmies()
	initPlayer()
	initBackground()
	initDeathAnims()
	initStars()
	initConfetti()
	initSound()
	initFinishBanners()
end

function startGame()
	startTutorial()
	startArmies()
	startPlayer()
	startStars()
	startSplash()
	startConfetti()
	startFinishBanners()

	fadeFromBlack = true
	fadeToBlack = false
	blackOpacity = 1

	-- stop assumes rewind
	Sound.mainSong:rewind()
	Sound.mainSong:play()
	startTimer()
end

function initFinishBanners()
	Banners = {{},{}}
	Banners[1].img = love.graphics.newImage("img/winnbanner_yellow2.png")
	Banners[2].img = love.graphics.newImage("img/winnbanner_red2.png")
end

function startFinishBanners()
	Banners[1].ofs = - Banners[1].img:getWidth()
	Banners[1].dest = screenSize.x / 2 - Banners[1].img:getWidth()/2
	Banners[2].dest = screenSize.x / 2 - Banners[2].img:getWidth()/2
	Banners[1].yofs = screenSize.y / 2 - Banners[1].img:getHeight()*0.7
	Banners[2].yofs = screenSize.y / 2 - Banners[2].img:getHeight()*0.7
	Banners[2].ofs = screenSize.x
	Banners[1].dir = 1
	Banners[2].dir = -1
	Banners.speed = 2500
	Banners.current = 0
	NoTouchTime = 3
end

function updateFinishBanners(dt)
	if Banners.current ~= 0 then
		local b = Banners[Banners.current]
		if b.dir < 0 and b.ofs <= b.dest then
			b.ofs = b.dest
		elseif b.dir > 0 and b.ofs >= b.dest then
			b.ofs = b.dest
		else
			b.ofs = b.ofs + Banners.speed * b.dir * dt
		end

		if NoTouchTime > 0 then
			NoTouchTime = NoTouchTime - dt
		end
	end
end

function drawFinishBanners()
	if Banners.current ~= 0 then
		local b = Banners[Banners.current]

		love.graphics.setColor(255,255,255)
		love.graphics.draw(b.img, b.ofs, b.yofs)
	end
end

function startSplash()
	gameState = 0
	splash.pos = 0
	splash.speed = 0
end

function love.draw()
	-- background
	drawBackground()

	if gameState ~= 0 then
		drawArmies()
		drawStars()
		drawConfetti()
	end

	drawArmiesBar()
	drawForeground()

	if splash.pos > -screenSize.y * 2 then
		love.graphics.setColor(255,255,255)
		love.graphics.draw(splash.img, 0, splash.pos)
	end

	drawTutorial()

	if gameState == 3 then
		drawWinScreen()
	end
	drawBlackOverlay()
end

function drawWinScreen()
	drawFinishBanners()
end

function updateWinScreen(dt)
	if armies.balance <= 0 then
		Banners.current = 2
	elseif armies.balance >= armies.maxbalance then
		Banners.current = 1
	end

	updateFinishBanners(dt)
end

function initBackground()
	bgImg = love.graphics.newImage("img/BG2.png")
	fgImg = love.graphics.newImage("img/GUI_topbar2.png")
	splash = {}
	splash.img = love.graphics.newImage("img/MOTAFUKIN_START_POPUP.png")
	splash.pos = 0
	splash.speed = 0
end

function drawBackground()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(bgImg, 0, 0)
end

function drawForeground()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(fgImg, 0, 0)
end

function love.update(dt)
	-- skip below 10FPS
	if dt > 0.1 then return end

	if gameState ~= 0 then
		updateArmies(dt)
		updateStars(dt)
		updateConfetti(dt)
	end

	if splash.pos > -screenSize.y * 2 then
		splash.pos = splash.pos + splash.speed * dt
	end

	updateTutorial(dt)

	if gameState == 3 then
		updateWinScreen(dt)
	end

	updateFadeOverlay(dt)
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function drawBlackOverlay()
	if fadeToBlack or fadeFromBlack then
		love.graphics.setColor(0,0,0,255 * blackOpacity)
		love.graphics.rectangle("fill",0,0,screenSize.x, screenSize.y)
	end
end

function updateFadeOverlay(dt)
	if fadeFromBlack then
		blackOpacity = blackOpacity * math.pow(0.97, dt * 60)
		if blackOpacity < 0.01 then
			fadeFromBlack = false
		end
	end

	if fadeToBlack then
		blackOpacity = increaseExponential(dt, blackOpacity, 0.9)
		if blackOpacity >= 1 then
			fadeToBlack = false
			startGame()
		end
	end
end

function beginGame()
	gameState = 1
	splash.speed = -600
	Sound.mainSong:rewind()
	Sound.mainSong:play()
	startTimer()
end

function love.mousepressed(x, y)
	click()
end

function click()
	if gameState == 0 then
		if Tutorial.visible and Tutorial.currentPage <= Tutorial.count then
			tutorialClicked()
		else
			beginGame()
		end
	elseif gameState == 3 and NoTouchTime <= 0 then
		fadeToBlack = true
	end
end

function love.keypressed(key)
	if key == "escape" then
		love.event.push("quit")
	end

	if key=="return" then
		if love.keyboard.isDown("ralt") or love.keyboard.isDown("lalt") then
			isFullscreen = not isFullscreen
			if love.graphics.toggleFullscreen then
				love.graphics.toggleFullscreen()
			elseif love.window.setFullscreen then
				love.window.setFullscreen(isFullscreen)
			end
		end
	end

	if key == "space"  then
		click()
	end

end
