function initDeathAnims()
	prepareDeathFrames()
end


function prepareDeathFrames()
	local img = love.graphics.newImage("img/death_white.png")

	DeathAnims = {{},{}}
	DeathAnims.batches = {}
	DeathAnims.batches[1] = love.graphics.newSpriteBatch(img,10*16,"static") -- usagehint?

	for j=0,9 do
		DeathAnims[1][j+1] = {}
		for i=0,15 do
			table.insert(DeathAnims[1][j+1], 
				love.graphics.newQuad(i*128, j*128, 128, 128, img:getWidth(), img:getHeight()))
		end
	end

	local img = love.graphics.newImage("img/death_black.png")
	DeathAnims.batches[2] = love.graphics.newSpriteBatch(img,10*16,"static") -- usagehint?

	for j=0,9 do
		DeathAnims[2][j+1] = {}
		for i=0,15 do
			table.insert(DeathAnims[2][j+1], 
				love.graphics.newQuad(i*128, j*128, 128, 128, img:getWidth(), img:getHeight()))
		end
	end
end

function addDeathAnim(position, color, ssize)
	table.insert(armies.list, {
		isDeathAnim = true,
		baseNdx = color,
		sizeNdx = ssize,

		pos = { x = math.floor(position.x), y = math.floor(position.y) },
		frameNdx = 0,
		frameTime = 0,
		frameDelay = 0.05
		})
end

function updateDeathAnim(anim, dt)
	anim.frameTime = anim.frameTime - dt
	while anim.frameTime <= 0 do
		anim.frameTime = anim.frameTime + anim.frameDelay
		anim.frameNdx = anim.frameNdx + 1
		if anim.frameNdx > 16 then
			anim.dead = true
		end
	end
end

function drawDeathAnim(anim)
	love.graphics.setColor(255,255,255)
	drawBatch(DeathAnims.batches[anim.baseNdx], DeathAnims[anim.baseNdx][anim.sizeNdx][anim.frameNdx], anim.pos.x, anim.pos.y, 0, 1, 1, 64, 64)
end